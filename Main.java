package org.example;

import java.util.Scanner;

abstract class Operation {
    public abstract int calculate(int num1, int num2);
}

class AddOperation extends Operation {
    @Override
    public int calculate(int num1, int num2) {
        return num1 + num2;
    }
}

class SubtractOperation extends Operation {
    @Override
    public int calculate(int num1, int num2) {
        return num1 - num2;
    }
}

class MultiplyOperation extends Operation {
    @Override
    public int calculate(int num1, int num2) {
        return num1 * num2;
    }
}

class DivideOperation extends Operation {
    @Override
    public int calculate(int num1, int num2) {
        if (num2 == 0) {
            throw new ArithmeticException("Ошибка: попытка деления на 0");
        }
        return num1 / num2;
    }
}

class PowerOperation extends Operation {
    @Override
    public int calculate(int num1, int num2) {
        return (int) Math.pow(num1, num2);
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите первое число:");
        int num1 = scanner.nextInt();

        System.out.println("Введите второе число:");
        int num2 = scanner.nextInt();

        System.out.println("Введите операцию (+, -, *, /, ^):");
        String operation = scanner.next();

        Operation op;
        switch (operation) {
            case "+":
                op = new AddOperation();
                break;
            case "-":
                op = new SubtractOperation();
                break;
            case "*":
                op = new MultiplyOperation();
                break;
            case "/":
                op = new DivideOperation();
                break;
            case "^":
                op = new PowerOperation();
                break;
            default:
                System.out.println("Ошибка: введена неверная операция");
                return;
        }

        int result;
        try {
            result = op.calculate(num1, num2);
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("Результат в BIN: " + Integer.toBinaryString(result));
        System.out.println("Результат в OCT: " + Integer.toOctalString(result));
        System.out.println("Результат в DEX: " + result);
        System.out.println("Результат в HEX: " + Integer.toHexString(result));
    }
}
